#!/usr/bin/php -q
<?php

error_reporting(E_ALL);
set_time_limit(0);

function writeLog($s){
	$s = sprintf("%s: %s\n", date('r'), $s);
	file_put_contents('/var/log/ipmac-server.log', $s, FILE_APPEND);
}

class Server{
	private $address, $port, $server, $client;

	private function listen(){
		$this->server = stream_socket_server("tcp://{$this->address}:{$this->port}", $errno, $errorMessage);

		if ($this->server === false) {
			throw new UnexpectedValueException("Could not bind to socket: $errorMessage");
		}

		return true;
	}

	public function read(){
		if (!$this->server && !$this->listen()) {
			return null;
		}
		$this->client = @stream_socket_accept($this->server);

		if ($this->client) {
			$data = trim(fgets($this->client));
			echo ">$data\n";
			return json_decode(trim($data));
		}

		return null;
	}

	public function write($data){
		if(!$this->client){
			return false;
		}

		$data = json_encode($data);
		echo "<$data\n";
		fwrite($this->client, $data."\r\n");

		return true;
	}

	public function close(){
		try {
			if($this->client) {
				fclose($this->client);
			}
		}catch(Exception $e){}
	}

	public function unbind(){
		try {
			if($this->server) {
				fclose($this->server);
			}
		}catch(Exception $e){}
	}

	function __construct($address, $port){
		$address = gethostbyname($address);
		$this->address = $address;
		$this->port = $port;
	}

	function __destruct(){
		$this->unbind();
		$this->close();
	}
}

function getMac($host){
	$output = array();
	exec("/sbin/arp -a -n | grep '${host}[^0-9]' | awk '{ print $4 }'", $output);
	$mac = @$output[0];

	writeLog(sprintf('Getting mac of %s: %s', $host, $mac));

	return $mac;
}

function banIpMac($ip, $mac){
	writeLog(sprintf('Banning %s %s', $ip, $mac));

	exec(sprintf('sudo /usr/sbin/ipset -D ipmacs %s,%s', $ip, $mac));
}

function unbanIpMac($ip, $mac){
	writeLog(sprintf('Unbanning %s %s', $ip, $mac));

	exec(sprintf('sudo /usr/sbin/ipset -A ipmacs %s,%s', $ip, $mac));
}

function listRules(){
	writeLog(sprintf('Listing rules'));
	exec('/usr/sbin/ipset -L | grep "," | grep 172', $out);

	if(count($out)){
		$res = array_combine($out, array_fill(0, count($out), 0));
	}else{
		$res = array();
	}

	return $res;
}

$s = new Server('0.0.0.0', '10001');
while(true){
	$msg = $s->read();
	if(!$msg){
		continue;
	}

	switch($msg->method){
		case 'getMac':
			$s->write(getMac($msg->ip));
			break;
		case 'ban':
			banIpMac($msg->ip, $msg->mac);
			break;
		case 'unban':
			unbanIpMac($msg->ip, $msg->mac);
			break;
		case 'listRules':
			$s->write(listRules());
			break;
	}
	$s->close();
}

?>
