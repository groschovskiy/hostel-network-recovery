#!/usr/bin/perl

#USER CONTROL SCRIPT by Steel_Rat (30.11.2010)
use DBI;

#MYSQL Account
my $dbhost = "localhost";
my $dbname = "hnabase";
my $dbuser = "admin-script";
my $dbpass = "83marathon84";

#Current Day Number
my $curday = `date +%d%H%M | sed s/^0//g`;

#my $curday = 0;
my $cmonth = `date +%m | sed s/^0//g`;
my $dmonth = $cmonth;

#connect DB
my $db = DBI->connect("DBI:mysql:$dbname:$dbhost", $dbuser, $dbpass);

printf("Current month %s\n", $cmonth);

my $banned_count = 0;

#BAN CYCLE
	my ($gban,$gipban, $gmacban);
	my $globalban = $db->prepare("SELECT hna_users.user_id,hna_users.lastip,hna_users.lastmac FROM hna_users LEFT JOIN hna_pays ON hna_users.user_id=hna_pays.user_id WHERE ((( hna_pays.$cmonth='0' and hna_pays.$dmonth='0') OR hna_pays.connect='0' ) AND  hna_users.status='0') AND ( hna_users.note NOT LIKE '%dontban%' OR hna_users.note is NULL);");
	$globalban->execute;
	$globalban->bind_columns(undef, \($gban, $gipban, $gmacban));
	while ($globalban->fetch) {
		$banned_count += 1;
		$gban_usr = $db->prepare("UPDATE hna_users SET status='1' WHERE user_id=$gban;");
		$gban_usr->execute;

		printf("User %d has been banned\n", $gban);
		$gban_log = $db->prepare("INSERT INTO hna_log_users (user_id,admin_id,time,action,message) VALUES ($gban,1,NOW(),4,'Пользователь автоматически забанен за неуплату');");
		$gban_log->execute;
		exec `ipset -D ipmacs $gipban,$gmacban 2> userctrl.err`;
	}
	$globalban->finish(); 

printf("Count of banned users: %d\n", $banned_count);

my $ubanned_count = 0;

#UNBAN CYCLE
my ($unban,$ipunban,$macunban);
my $unbanlist = $db->prepare("SELECT hna_users.user_id,hna_users.lastip,hna_users.lastmac FROM hna_users LEFT JOIN hna_pays ON hna_users.user_id=hna_pays.user_id WHERE ( hna_users.status='1' AND hna_pays.connect='1' AND ( hna_pays.$cmonth='1' OR hna_pays.$dmonth='1') ) OR ( hna_users.status = '1' AND hna_users.note LIKE '%dontban%' );");
$unbanlist->execute;
$unbanlist->bind_columns(undef, \($unban, $ipunban, $macunban));
my ($unban_usr,$unban_log);
while ($unbanlist->fetch) {
	$unbanned_count += 1;
	$unban_usr = $db->prepare("UPDATE hna_users SET status='0' WHERE user_id=$unban;");
	$unban_usr->execute;
	$unban_log = $db->prepare("INSERT INTO hna_log_users (user_id,admin_id,time,action,message) VALUES ($unban,1,NOW(),4,'Пользователь автоматически разбанен');");
	$unban_log->execute;
}
$unbanlist->finish();

#Kill DB connection
$rc = $db->disconnect;

printf("Count of unbanned: %d\n", $unbanned_count);
